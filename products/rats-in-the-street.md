---
layout: page
title: Rats in the Street
---

<img src="/assets/img/products/rats-in-the-street.jpg" style="max-width: 200px; float: right; padding: .25em;">
Butcher Block has been caught in the throes of crime. Businesses are being extorted, people are being assaulted, and homes are being robbed. At the heart of everything, is the Crystal Moth gang. Once thought of as a joke, unworthy of attention, the gang now terrorizes the district, and the city guard is unwilling, or unable, to help.

_Rats in the Street_ is an urban adventure for five characters of 3rd-level. When a street gang steps up its activities, the party is called in to help, but they must be careful, lest the gang lash out against the very people the PCs seek to aid.

<div class="product-link text-center">
  <a class="default-btn" href="https://www.drivethrurpg.com/product/182844/Rats-in-the-Street-5e" target="drivethrurpg">Buy it now (5e)</a>
  <a class="default-btn" href="https://www.drivethrurpg.com/product/182874/Rats-in-the-Street-Pathfinder" target="drivethrurpg">Buy it now (Pathfinder)</a>
</div>
