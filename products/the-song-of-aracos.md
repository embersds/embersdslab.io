---
layout: page
title: The Song of Aracos
---

<img src="/assets/img/products/the-song-of-aracos.jpg" style="max-width: 200px; float: right; padding: .25em;">
_“Strahd isn’t a villain who remains out of sight until the final scene. He travels as he desires to any place in his realm, and the more often he encounters the characters, the better. The characters can and should meet him multiple times before the final encounter…”_ -- Curse of Strahd

This is the story of one of those encounters.

What secret lies hidden in the village of Aracos that draws the devil Strahd von Zarovich? It is ancient, reaching all the way back to the days before Strahd became a vampire, before Barovia was torn from the mortal realm and cast into the Demiplane of Dread.

_The Song of Aracos_ is an adventure for the Dungeons and Dragons 5e Roleplaying Game. The module is optimized for five characters of 6th-level and is intended to be used in conjunction with the Curse of Strahd hardcover adventure by Wizards of the Coast.

In _The Song of Aracos_, your player’s characters come face-to-face with Strahd as the vampire struggles to understand the compulsion pulling him to the village of Aracos. Whether they choose to help, or stand in his way, depends on them, and the fates.

<div class="product-link text-center">
  <a class="default-btn" href="https://www.dmsguild.com/product/179031/The-Song-of-Aracos" target="drivethrurpg">Buy it now (5e)</a>
</div>
