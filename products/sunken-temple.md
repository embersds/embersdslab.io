---
layout: page
title: Sunken Temple
---

<img src="/assets/img/products/sunken-temple.jpg" style="max-width: 200px; float: right; padding: .25em;">
In the forbidden places of this world, where civilization has yet to penetrate, there dwell horrors of a primeval age. Scholars speak of these elder things - these _morguul_ things - only hesitantly, for to drink too deeply of their lore is to stain one’s very soul.

_Sunken Temple_ is an adventure for the 5e Roleplaying Game, intended for five characters of 6th level. The adventure takes place in a mysterious range of never-before-seen mountains and is suitable for use in any setting. _Sunken Temple_ is inspired by the works of _H.P. Lovecraft, particularly The Case of Charles Dexter Ward, The Shadow Over Innsmouth,_ and _At the Mountains of Madness._

<div class="product-link text-center">
  <a class="default-btn" href="https://www.drivethrurpg.com/product/212516/Sunken-Temple-5e" target="drivethrurpg">Buy it now (5e)</a>
  <a class="default-btn" href="https://www.drivethrurpg.com/product/212517/Sunken-Temple-Pathfinder" target="drivethrurpg">Buy it now (Pathfinder)</a>
  <a class="default-btn" href="https://www.drivethrurpg.com/product/212519/Sunken-Temple-WOIN" target="drivethrurpg">Buy it now (WOIN)</a>
</div>
