---
layout: page
title: The Book of Terniel
---

<img src="/assets/img/products/the-book-of-terniel.jpg" style="max-width: 200px; float: right; padding: .25em;">
Four factions struggle for the Book of Terniel, and in the middle of them: you! Choose a side or claim it for yourself.

The Book of Terniel is a Pathfinder Roleplaying Game compatible adventure for 4-5 1st-level characters. Players will enjoy several different types of gameplay, including old-fashioned dungeon crawls, wide-open exploration, and plenty of opportunity for roleplaying, investigation, and diplomacy.

Here's what you get:

- A **226-page adventure** that pits your players against goblins, undead, demons, giants, and more as they struggle to recover the stolen Book of Terniel.
- A **35-page players guide** that includes tips on making characters, new campaign traits, a new deity, an overview of the area where the adventure takes place, two new playable races, eight pregenerated characters, and four wanted posters.
- A **4-page player handout**, Walcott's Journal wherein they can take a stroll through the mind of a cultist to get a peek into the inner workings of the Kaj'Talel cult, one of the key factions in the adventure.

<div class="product-link text-center">
  <a class="default-btn" href="https://www.drivethrurpg.com/product/140711/The-Book-of-Terniel" target="drivethrurpg">Buy it now (Pathfinder)</a>
</div>
