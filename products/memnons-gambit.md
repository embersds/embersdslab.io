---
layout: page
title: Memnon's Gambit
---

<img src="/assets/img/products/the-song-of-aracos.jpg" style="max-width: 200px; float: right; padding: .25em;">
_Memnon's Gambit_ is a short adventure intended to be used as an introduction to the **<a href="http://www.woinrpg.com" target="WOIN">What’s O.L.D. is N.E.W. (W.O.I.N.)</a>** roleplaying game. In this adventure, the characters move between the distant past, the present, the far future, and the end of time. Each of these periods serves to highlight one aspect of the W.O.I.N. game: O.L.D. for the past, N.O.W. for the present, N.E.W. for the future.

_Memnon's Gambit_ is meant to be played in a 4-hour slot and includes 6 pregenerated characters.

_At the beginning of time, the span of every mortal was written in the Book of Cilo. Around this tome sprang up myths and legends, some even believing that if their ending could be erased from theBook of Cilo, they would live forever. They were right._

_By the middle of the 21st century, cancer and most diseases had been entirely eradicated. A little-known scientist named Robert Gibbons was credited with this momentous accomplishment. Gibbons vanished suddenly in the year 2015, but the work he left behind changed the world forever._

_In the early 35th century, humanity was flourishing. Colonies on the Moon and Mars were thriving, and at long last, scientists were on the verge of breakthroughs that would enable faster than light travel. Then came the Human Extinction virus, H.E.X. It swept across the planet and through the colonies like fire on a prairie. Humanity ended._

_At the end of time, Memnon sits alone, brooding. Words cannot capture his anguish, alone for longer than life existed. He seeks only death, but his end has been erased from the Book of Cilo._

<div class="product-link text-center">
  <a class="default-btn" href="https://www.drivethrurpg.com/product/196033/Memnons-Gambit-WOIN" target="drivethrurpg">Buy it now (WOIN)</a>
</div>
