---
layout: page
title: Embers RPG Vault
---

![Embers RPG Vault](/assets/img/products/embersrpgvault.jpg)

With **Embers RPG Vault** you can quickly create and update characters for your favorite roleplaying games. Our easy to use character sheets are tailored to the game, with most common calculations built right in, allowing you focus on your character, while we take care of the math!

All of us would rather be playing the game rather than entering rules, and **Embers RPG Vault** lets you do just that. We've already entered hundreds, maybe even thousands, of stock rules so that you don't have to, and more go in every day! Of course, if you're anything like us, you probably want to add your own house rules, that's no problem, enter them once and use them everywhere! Work less, play more!

Do you ever get tired of trying to remember what stat bumps what skill? How to calculate your saving throw bonus? What modifier comes along with your 18 Strength? Stop. Just stop. We've built integrated dice rolling right into the character sheet. Free your mind from the shackles of rule minutiae, **Embers RPG Vault** can do that work for you.

<div class="product-link text-center">
  <a class="default-btn" href="https://embersrpgvault.com" target="_blank">Go to Embers RPG Vault now</a>
</div>
