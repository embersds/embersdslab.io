---
layout: page
title: Yrisa's Nightmare
---

<img src="/assets/img/products/yrisas-nightmare.jpg" style="max-width: 200px; float: right; padding: .25em;">
Highhouse Yroden has been cursed. Apparitions stalk the streets, gnolls lurk in the wilds, and a dragon has taken interest in the village. Yet, the curse runs deeper. Nightmares plague townsfolks’ dreams and madness grips their waking minds. To make matters worse, just when she’s needed most, their wise woman has left.

**Yrisa’s Nightmare** is an adventure for the 5e or Pathfinder Roleplaying Game, intended for four characters of levels 2-3. In **Yrisa’s Nightmare**, the player characters are drawn into a curse afflicting Highhouse Yroden where, to survive, they must unravel the mystery and rescue the young oracle at its center.

<div class="product-link text-center">
  <a class="default-btn" href="https://www.drivethrurpg.com/product/175632/Yrisas-Nightmare-5e" target="drivethrurpg">Buy it now (5e)</a>
  <a class="default-btn" href="https://www.drivethrurpg.com/product/175878/Yrisas-Nightmare-Pathfinder" target="drivethrurpg">Buy it now (Pathfinder)</a>
</div>
