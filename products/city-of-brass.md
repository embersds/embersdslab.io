---
layout: page
title: City of Brass
---

![City of Brass](/assets/img/products/cityofbrass.png)
Our flagship product is City of Brass. An interactive, online tool that delivers the total gaming experience to players and gamemasters alike.

City of Brass is a fully-featured app specifically designed to manage the mechanics of pen-and-paper games allowing you to focus on what matters:
{ playing that game }

### What are you waiting for?

Begin exploring City of Brass now with a Free account which gives you

- 2 character slots
- 2 creature slots
- 20 custom rules.
- Unlimited access to our stock content.
- The ability to participate in Campaigns and ActivePlay.

The Free Account will also allow you to try out other City of Brass features including World Builder, Story Builder, and Campaign manager.

When you are ready to move to the head of the table and gain unlimited access to City of Brass you can subscribe for a Premium account!

<div class="product-link text-center">
  <a class="default-btn" href="https://cityofbrass.io" target="_blank">Go to City of Brass now</a>
</div>
