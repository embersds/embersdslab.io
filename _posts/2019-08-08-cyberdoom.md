---
layout: post
title: Dark Alliance Studios and Embers Design Studios Announce Cyberdoom Partnership 
author: Lucas Curell
excerpt_separator:  <!--more-->
tags:
  - cyberdoom
  - publishing
---

Dark Alliance Studios and Embers Design Studios today announced a partnership to bring Cyberdoom to market. The partnership gives the creative team behind Cyberdoom access to the experience of Embers Design Studios who have a history of successful projects.

<!--more-->

The world of Cyberdoom is a dark place, where evil has triumphed. The scattered remnants of mankind struggle to find a foothold against the despotic King of Trash City. Served by the Telecommunicator who lives near the stars. The Telecommunicator sees all, hears all, and knows all. Yet all hope is not lost. In Trash City a hero has arisen and around her the Resistance rallies while elsewhere, the Protectors of the Light, taught by angels, stir.  

The Cyberdoom worldbook will explore this exotic environment, introducing readers to the people and places of Cyberdoom: from Trash City with its King, to the “blues” who fight against him, to the fallen angels and demons that stalk the world. Illustrated in gorgeous full-page artwork and including short stories and comic pages, the worldbook brings Trash City to life. 

The Cyberdoom worldbook will be available in English and Spanish. 

“I was thrilled when Dark Alliance Studios asked for help with Cyberdoom. The project pages I had seen were astonishing and the dark and gritty world of Cyberdoom is something I’d love to be involved with.” said Lucas Curell, Embers Design Studios, Managing Partner. 

### About Dark Alliance Studios

Dark Alliance Studios is a partnership of creators and illustrators. Their first major publication will be the Cyberdoom worldbook. Their work can also be seen in Mitos y Leyendas, Aggressive Impact, Fuel Games, Embers Design Studios, and others. 

### About Embers Design Studios 

Embers Design Studios develops web and mobile apps and authors games for the pen-and-paper roleplaying game market. Thousands of gamers use the company’s online tools which include The City of Brass and EmbersRPGVault and their publications can be found on DriveThruRPG. 
