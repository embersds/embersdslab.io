---
layout: post
title: Where it all started and where we are today
author: Daniel Kay
excerpt_separator:  <!--more-->
tags:
  - city of brass
  - publishing
---

Almost 5 years ago Lucas came to me with a grand idea.  Why don’t we build the ultimate gaming app? He figured that between his business skills and my development skills we should be able to pull it off.

<!--more-->

### City of Brass Pre-Launch

He really didn’t have to sell me on it.  I was pretty much sold on the idea after his first sentence or two.  In fact, I was already trailing off thinking about what would be involved and what it looked like.  Let me tell you, what we both thought it would look like on that day and what it turned out to be when we launched City of Brass are NOT the same thing.  It definitely captured all of the key elements we were looking for.  Wold Building, Campaign Management, fully custom NON game specific character sheets, and tools for a GM to enter their own rules and monsters.

So, with an idea and a plan, we dove in head first. Formed our business, had weekly meetings, spent our own money, got an alpha version of the app in place so our own gaming group could start using it as early as possible (still feel bad a tester lost 4 hours of data entry because of my bad delete setup, but hey, its alpha, his pain was our customers gain).

Fast forward a year and a half. Launch day for City of Brass in the summer of 2015 was a pretty stressful day.  Lucas had spent months discussing it, promoting it and down right talking it up. Now it was time to launch and see if our time and effort was going to pay off.

### City of Brass Post Launch

It is safe to say our launch party (we had one) was not fully funded by our new massive income from City of Brass. Pretty sure I had to pay for my own beer, but thats what happens when you start your own business.

Our initial response to the City was certainly not as good as we had hoped.  We figured, if we could get a respectable sign-up count on launch day, we could double down and build it up and attract even more players and GMs. Unfortunately that didn’t happen. We certainly had the sign-ups we were hoping for, but the conversion rate of people trying the app vs those deciding it was worth paying for was no where near enough.  

### Where did we go wrong?
We believe there were four areas that have contributed to the City's lack of adoption.

1. Too complicated. With great flexibility comes great complexity. We tried to make it too flexible and in turn failed to attract any sort of casual player.
2. Expecting players to pay the same thing as a GM. Players are not interested in supporting their GM financially so they see no reason to pay the same thing for a product that benefits the GM more than them.  At least thats how they look at it.
3. Trying to be the end-all be-all with one app. Instead of targeting the GM or Players specifically and limited game systems, we tried to acomodate everyone and everything.
4. We are not the best marketing guys.  Lets face it, if people dont know about you, they cant buy your product.

### So now what?
We  believe the City is a solid product and plan to run it so long as there is enough interest in it to pay for it.  We still use it for some of our stuff. However, we have decide to focus our attention else where. 

First off, we have built a new character tool that only targets 5e and Pathfinder. We decided it just too difficult to try and target game systems beyond those two and would rather focus on making a great character app for the majority of players vs a mediocre app for all players. We plan to release it to the public sometime this summer.

We have also decided to focus more on publishing and using both publishing and Kickstarter to help drive awareness to our other products.  We have slowly built a following and believe focusing on that area will help us attact people to our applications.

### Stay informed
The best way to stay up to date with what we are doing is to join our mailing list. There you will hear about both our publishing products and our web applications.