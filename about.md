---
layout: page
title: About
sidebar_link: true
sidebar_sort_order: 1
---

At Embers Design Studios we develop web and mobile applications for business and
personal use, but that’s not who we are, that’s just what we do. We are the company
you want to work for, the one that doesn’t put profits before people and understands
that, just like us, you’re trying to make a go of it in this world we all share.

We came right out of corporate America. We loved our jobs, but didn’t want to rely
on others for our future wellbeing, and so Embers Design Studios was born. With
more than 40 years business experience to draw upon, and a diverse skillset, we
set out to make something bigger than either of us.

For now, our focus is entirely on apps for pen-and-paper role-playing games, but
that is only where we are starting. We chose this as a start because we’ve been
playing these games since we were 10 years old – and that was a long time ago now.
The passion we have for gaming and the love we have for our work is what makes all
the difference in our designs. Our product is under development now, check back
here for updates.

Our company’s headquarters is in the little town of Freeland, Michigan. If you’ve
ever heard of Detroit, you can find us about 120 miles north and west. You can
send us a letter if you like, but it’s faster to just drop an email to [info@embersds.com](mailto:info@embersds.com).
