---
layout: page
title: Products
permalink: /products/
sidebar_link: true
sidebar_sort_order: 2
---

We offer two categories of products.  Plushed products and web applications. All of our published products are offered through [DriveThruRPG.com](http://www.drivethrurpg.com/browse/pub/7632/Embers-Design-Studios).

### Publishing
<ul class="list-unstyled">
{% for item in site.data.publishing %}
  <li>
    <strong><a href="/products/{{ item.page }}">
      {{ item.name }}
    </a></strong><br>
    {{ item.description }}
  </li>
{% endfor %}
</ul>


### Applications
<ul class="list-unstyled">
{% for item in site.data.applications %}
  <li>
    <strong><a href="/products/{{ item.page }}">
      {{ item.name }}
    </a></strong><br>
    {{ item.description }}
  </li>
{% endfor %}
</ul>