---
layout: page
title: Blog
permalink: /blog/
sidebar_link: true
sidebar_sort_order: 3
---

{% for post in site.posts %}
<h2>
  <a href="{{ post.url }}">{{ post.title }}</a><br>
</h2>
<span class="post-date">{{ post.date | date: '%B %d, %Y' }} • {{ post.author }}</span>
{{ post.excerpt }}
<a class="default-btn" href="{{ post.url }}">Read More</a>
<hr>
{% endfor %}
